# 项目简介

CodeBuilder可以帮助你快速生成模板文件，目前支持mysql、oracle、sql server数据库。

您可以自己制作代码模板并添加到模板目录，帮助您可以应付各种开发场景。

# 使用建议

**注意：本代码生成器已经提供了一套示例代码**

1. 数据库的名称全部小写，名称要见名之意，多个单词，用“\_”分隔，例如：store
2. 数据库表名称全部小写，名称要见名之意，多个单词，用“\_”分隔，表备注是必须要写的，例如：tb\_goods\_item、商品表
3. 数据库字段名全部小写，名称要见名之意，多个单词，用“\_”分隔，列备注是必须要写的，例如：goods_name、商品名称

# 替换符

## 1、全局替换符

**注意：“全局替换符”可以在任意模板中使用**

| 替换符代码          | 替换符含义    | 渲染后内容                                |
| ------------------- | ------------------ | ------------------------------------------ |
| [project]           | 工程名称           | store                           |
| [projectComment]    | 工程描述           | 在线商城 |
| [projectAuthor]     | 工程作者           | caochenlei                                 |
| [projectVersion]    | 工程版本           | 1.0.0                         |
| [packageName]       | 包名               | io.caochenlei.store |
| [path_all]          | 全路径包名         | io/caochenlei/store                      |
| [path_1]            | 1级包名            | io                                        |
| [path_2]            | 2级包名            | caochenlei                        |
| [path_3]            | 3级包名            | store                              |
| [path_n]            | n级包名            | ...                                        |
| [dbIp]              | 数据库IP           | 127.0.0.1                                  |
| [dbPort]            | 数据库端口         | 3306                                       |
| [dbName]            | 数据库名称         | store                                  |
| [dbUrl]             | 数据库地址         | jdbc:mysql://127.0.0.1:3306/store |
| [dbUser]            | 数据库用户         | root                                       |
| [dbPassword]        | 数据库密码         | 123456                            |
| [dbType]            | 数据库类型         | MYSQL                                      |
| [dbDriverName]      | 数据库驱动         | com.mysql.jdbc.Driver                      |
| [dbDataSourceType]  | 数据库数据源类型   | com.alibaba.druid.pool.DruidDataSource |
| [dbDialect]         | 数据库方言         | DbType.MYSQL |
| [dbGenerator]       | 数据库主键生成策略 | IdType.AUTO |

## 2、表级替换符

**注意：“表级替换符”可以在任意模板中使用**

| 替换符代码          | 替换符含义                               | 渲染后内容 |
| ------------------- | ---------------------------------------- | ------------- |
| [tableName]         | 表名称                                   | tb_goods_item |
| [tableComment]      | 表描述                                   | 商品表       |
| [table1]            | 表名，小写开头                           | tb_goods_item |
| [Table1Upper]       | 表名，大写开头                           | Tb_goods_item |
| [table2]            | 表名，小写开头，驼峰格式                 | tbGoodsItem   |
| [Table2Upper]       | 表名，大写开头，驼峰格式                 | TbGoodsItem   |
| [table3]            | 表名，小写开头，去除前缀，驼峰格式       | goodsItem     |
| [Table3Upper]       | 表名，大写开头，去除前缀，驼峰格式       | GoodsItem     |
| [tablePrimaryKeyName]         | 表主键，名称                                   | order_id          |
| [tablePrimaryKeyComment]      | 表主键，备注                                   | 商品主键          |
| [tablePrimaryKeyType]         | 表主键，数据库类型                             | BIGINT            |
| [tablePrimaryKeyLanguageType] | 表主键，编程语言类型                           | Long              |
| [tablePrimary1]               | 表主键，小写开头                               | order_id          |
| [TablePrimary1Upper]          | 表主键，大写开头                               | Order_id          |
| [tablePrimary2]               | 表主键，小写开头，驼峰格式                     | orderId           |
| [TablePrimary2Upper]          | 表主键，大写开头，驼峰格式                     | OrderId           |
| [tablePrimary3]               | 表主键，小写开头，去除前缀，驼峰格式           | id                |
| [TablePrimary3Upper]          | 表主键，大写开头，去除前缀，驼峰格式           | Id                |

## 3、列级替换符

**注意：“列级替换符”只能在表级模板中使用**

| 替换符代码           | 替换符含义                           | 渲染后内容    |
| -------------------- | ------------------------------------ | ------------- |
| [columnName]         | 列名称                               | tb_goods_name |
| [columnComment]      | 列描述                               | 商品名称      |
| [columnType]         | 字段，数据库类型                     | varchar       |
| [columnLanguageType] | 字段，编程语言类型                   | String        |
| [column1]            | 字段名，小写开头                     | tb_goods_name |
| [Column1Upper]       | 字段名，大写开头                     | Tb_goods_name |
| [column2]            | 字段名，小写开头，驼峰处理           | tbGoodsName   |
| [Column2Upper]       | 字段名，大写开头，驼峰处理           | TbGoodsName   |
| [column3]            | 字段名，小写开头，去除前缀，驼峰处理 | goodsName     |
| [Column3Upper]       | 字段名，大写开头，去除前缀，驼峰处理 | GoodsName     |

## 4、列级特殊符

**注意：“列级特殊符”只能在列级模板中使用**

| 替换符代码            | 替换符含义   |
| --------------------- | ------------ |
| [newLine]             | 插入一个换行 |
| [nowrap]              | 去掉所有换行 |
| [deleteCharAtLastOne] | 删除最后一个字符 |
| [deleteCharAtLastTwo] | 删除最后两个字符 |

# 联系作者

Email：774908833@qq.com