package [packageName].mapper;

import [packageName].domain.[Table2Upper];
import java.util.List;

public interface [Table2Upper]Mapper {
    int insert([Table2Upper] [table2]);

    int deleteByPrimaryKey([tablePrimaryKeyLanguageType] [tablePrimary2]);

    int updateByPrimaryKey([Table2Upper] [table2]);

    [Table2Upper] selectByPrimaryKey([tablePrimaryKeyLanguageType] [tablePrimary2]);

    List<[Table2Upper]> selectList([Table2Upper] [table2]);
}