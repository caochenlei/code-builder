package [packageName].service;

import com.github.pagehelper.PageInfo;
import [packageName].domain.entity.[Table2Upper];

import java.util.List;

public interface [Table2Upper]Service {
    int add([Table2Upper] [table2]);

    int batchAdd(List<[Table2Upper]> [table2]List);

	int batchAddOrUpdate(List<[Table2Upper]> [table2]List);

    int deleteBy[TablePrimary2Upper]([tablePrimaryKeyLanguageType] [tablePrimary2]);

    int updateBy[TablePrimary2Upper]([Table2Upper] [table2]);

    [Table2Upper] getBy[TablePrimary2Upper]([tablePrimaryKeyLanguageType] [tablePrimary2]);

    List<[Table2Upper]> selectList([Table2Upper] query);

	List<[Table2Upper]> getList();

    PageInfo<[Table2Upper]> getPage([Table2Upper] query, Integer pageNum, Integer pageSize);
}