package [packageName].service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import [packageName].domain.entity.[Table2Upper];
import [packageName].mapper.[Table2Upper]Mapper;
import [packageName].service.[Table2Upper]Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class [Table2Upper]ServiceImpl implements [Table2Upper]Service {
    @Resource
    private [Table2Upper]Mapper [table2]Mapper;

    @Transactional
    @Override
    public int add([Table2Upper] [table2]) {
        return [table2]Mapper.insert([table2]);
    }

	@Transactional
    @Override
    public int batchAdd(List<[Table2Upper]> [table2]List) {
		if (CollectionUtils.isEmpty([table2]List)) return 0;
        return [table2]Mapper.batchInsert([table2]List);
    }

	@Transactional
    @Override
    public int batchAddOrUpdate(List<[Table2Upper]> [table2]List) {
		if (CollectionUtils.isEmpty([table2]List)) return 0;
		return [table2]Mapper.batchInsertOrUpdate([table2]List);
    }

    @Transactional
    @Override
    public int deleteBy[TablePrimary2Upper]([tablePrimaryKeyLanguageType] [tablePrimary2]) {
        return [table2]Mapper.deleteByPrimaryKey([tablePrimary2]);
    }

    @Transactional
    @Override
    public int updateBy[TablePrimary2Upper]([Table2Upper] [table2]) {
        return [table2]Mapper.updateByPrimaryKey([table2]);
    }

    @Override
    public [Table2Upper] getBy[TablePrimary2Upper]([tablePrimaryKeyLanguageType] [tablePrimary2]) {
        [Table2Upper] [table2] = [table2]Mapper.selectByPrimaryKey([tablePrimary2]);
        if ([table2] == null) return null;
        List<[Table2Upper]> [table2]List = Arrays.asList([table2]);
        handleList([table2]List);
        return [table2]List.get(0);
    }

    @Override
    public List<[Table2Upper]> selectList([Table2Upper] query) {
        List<[Table2Upper]> [table2]List = [table2]Mapper.selectList(query);
        handleList([table2]List);
        return [table2]List;
    }

    @Override
    public List<[Table2Upper]> getList() {
        return selectList(null);
    }

    @Override
    public PageInfo<[Table2Upper]> getPage([Table2Upper] query, Integer pageNum, Integer pageSize) {
        if (pageNum != null && pageSize != null) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<[Table2Upper]> [table2]List = selectList(query);
        return new PageInfo<>([table2]List);
    }

    private void handleList(List<[Table2Upper]> [table2]List) {
        if (CollectionUtils.isEmpty([table2]List)) return;
        // Please write the code for processing the list here
		// todo
    }
}