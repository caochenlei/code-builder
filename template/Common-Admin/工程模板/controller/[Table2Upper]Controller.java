package [packageName].controller;

import com.github.pagehelper.PageInfo;
import [packageName].annotation.SysLogRecord;
import [packageName].domain.entity.[Table2Upper];
import [packageName].domain.vo.ResultVo;
import [packageName].service.[Table2Upper]Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@Api(tags = "[tableComment]接口")
public class [Table2Upper]Controller {
    @Resource
    private [Table2Upper]Service [table2]Service;

    @SysLogRecord("新增[tableComment]信息")
    @PreAuthorize("hasAuthority('sys:[table3]:add')")
    @ApiOperation("新增[tableComment]信息")
    @PostMapping("/api/[table2]/add")
    public ResultVo add(@RequestBody [Table2Upper] [table2]) {
        int rows = [table2]Service.add([table2]);
        if (rows > 0) {
            return ResultVo.success("添加成功！");
        } else {
            return ResultVo.fail("添加失败！");
        }
    }

    @SysLogRecord("批量新增[tableComment]信息")
    @PreAuthorize("hasAuthority('sys:[table3]:batchAdd')")
    @ApiOperation("批量新增[tableComment]信息")
    @PostMapping("/api/[table2]/batchAdd")
    public ResultVo batchAdd(@RequestBody List<[Table2Upper]> [table2]List) {
        int rows = [table2]Service.batchAdd([table2]List);
        if (rows > 0) {
            return ResultVo.success("批量添加成功！");
        } else {
            return ResultVo.fail("批量添加失败！");
        }
    }

    @SysLogRecord("根据主键删除[tableComment]")
    @PreAuthorize("hasAuthority('sys:[table3]:delete')")
    @ApiOperation("根据主键删除[tableComment]")
    @GetMapping("/api/[table2]/deleteBy[TablePrimary2Upper]")
    public ResultVo deleteBy[TablePrimary2Upper]([tablePrimaryKeyLanguageType] [tablePrimary2]) {
		int rows = [table2]Service.deleteBy[TablePrimary2Upper](id);
		if (rows > 0) {
			return ResultVo.success("删除成功！");
		} else {
			return ResultVo.fail("删除失败！");
		}
    }

    @SysLogRecord("根据主键更新[tableComment]")
    @PreAuthorize("hasAuthority('sys:[table3]:update')")
    @ApiOperation("根据主键更新[tableComment]")
    @PostMapping("/api/[table2]/update")
    public ResultVo updateById(@RequestBody [Table2Upper] [table2]) {
		int rows = [table2]Service.updateBy[TablePrimary2Upper]([table2]);
		if (rows > 0) {
			return ResultVo.success("更新成功！");
		} else {
			return ResultVo.fail("更新失败！");
		}
    }

    @SysLogRecord("根据主键获取[tableComment]")
    @PreAuthorize("hasAuthority('sys:[table3]:update')")
    @ApiOperation("根据主键获取[tableComment]")
    @GetMapping("/api/[table2]/getBy[TablePrimary2Upper]")
    public ResultVo getBy[TablePrimary2Upper]([tablePrimaryKeyLanguageType] [tablePrimary2]) {
        [Table2Upper] [table2] = [table2]Service.getBy[TablePrimary2Upper](id);
        return ResultVo.successWith([table2]);
    }

    @SysLogRecord("获取全部[tableComment]记录")
    @ApiOperation("获取全部[tableComment]记录")
    @GetMapping("/api/[table2]/getList")
    public ResultVo getList() {
        List<[Table2Upper]> [table2]List = [table2]Service.getList();
        return ResultVo.successWith([table2]List);
    }

    @SysLogRecord("分页获取[tableComment]记录")
    @ApiOperation("分页获取[tableComment]记录")
    @GetMapping("/api/[table2]/getPage")
    public ResultVo getPage([Table2Upper] query, Integer pageNum, Integer pageSize) {
        PageInfo<[Table2Upper]> [table2]Page = [table2]Service.getPage(query, pageNum, pageSize);
        return ResultVo.successWith([table2]Page);
    }
}