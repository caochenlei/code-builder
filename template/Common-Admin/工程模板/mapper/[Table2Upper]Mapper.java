package [packageName].mapper;

import [packageName].domain.entity.[Table2Upper];
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface [Table2Upper]Mapper {
    int insert([Table2Upper] [table2]);

    int batchInsert(List<[Table2Upper]> list);

	int batchInsertOrUpdate(List<[Table2Upper]> list);

    int deleteByPrimaryKey([tablePrimaryKeyLanguageType] [tablePrimary2]);

    int updateByPrimaryKey([Table2Upper] [table2]);

    [Table2Upper] selectByPrimaryKey([tablePrimaryKeyLanguageType] [tablePrimary2]);

    List<[Table2Upper]> selectList([Table2Upper] [table2]);
}