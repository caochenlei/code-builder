package com.caochenlei.codebuilder.utils;

import java.io.File;

public class SystemUtil {

	/**
	 * 功能说明：获取工程目录地址
	 * 
	 * @return
	 */
	public static String getProjectAbsolutePath() {
		return System.getProperty("user.dir");
	}

	/**
	 * 功能说明：获取默认创建地址
	 * 
	 * @return
	 */
	public static String getDefaultCreatePath() {
		return System.getProperty("user.dir") + "\\target\\code";
	}

	/**
	 * 功能说明：获取配置文件地址
	 * 
	 * @return
	 */
	public static String getConfigAbsolutePath() {
		return System.getProperty("user.dir") + "\\config";
	}

	/**
	 * 功能说明：根据名称获取配置文件地址
	 * 
	 * @param configName
	 * @return
	 */
	public static String getConfigAbsolutePathByName(String configName) {
		return System.getProperty("user.dir") + "\\config\\" + configName + ".xml";
	}

	/**
	 * 功能说明：根据名称获取配置文件对象
	 * 
	 * @param configName
	 * @return
	 */
	public static File getConfigFileByName(String configName) {
		return new File(System.getProperty("user.dir") + "\\config\\" + configName + ".xml");
	}

}
