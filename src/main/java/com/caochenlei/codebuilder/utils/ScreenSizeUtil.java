package com.caochenlei.codebuilder.utils;

import java.awt.Dimension;
import java.awt.Toolkit;

public class ScreenSizeUtil {

	private static Dimension screenSize;

	static {
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	}

	/**
	 * 功能说明：获取屏幕宽度
	 * 
	 * @return
	 */
	public static double getWidth() {
		return screenSize.getWidth();
	}

	/**
	 * 功能说明：获取屏幕高度
	 * 
	 * @return
	 */
	public static double getHeight() {
		return screenSize.getHeight();
	}

	/**
	 * 功能说明：获取屏幕宽度中心
	 * 
	 * @return
	 */
	public static double getWidthCenter() {
		return screenSize.getWidth() / 2.0;
	}

	/**
	 * 功能说明：获取屏幕高度中心
	 * 
	 * @return
	 */
	public static double getHeightCenter() {
		return screenSize.getHeight() / 2.0;
	}

}
