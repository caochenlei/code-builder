package com.caochenlei.codebuilder.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class PackageUtil {

	/**
	 * 功能说明：获取包名完整路径
	 * 
	 * @param packageName
	 * @return
	 */
	public static String getPathAll(String packageName) {
		return packageName.replaceAll("\\.", "/");
	}

	/**
	 * 功能说明：获取包名的子包名
	 * 
	 * @param packageName
	 * @return
	 */
	public static Map<String, String> getPathN(String packageName) {
		Map<String, String> map = new TreeMap<String, String>(new Comparator<String>() {
			@Override
			public int compare(String obj1, String obj2) {
				return obj1.compareTo(obj2);
			}
		});

		String[] split = packageName.split("\\.");
		map.put("path_n", split.length + "");
		for (int i = 0; i < split.length; i++) {
			map.put("path_" + (i + 1), split[i]);
		}

		return map;
	}

}
