package com.caochenlei.codebuilder.utils;

import java.util.List;
import java.util.Map;

import com.caochenlei.codebuilder.entity.Database;
import com.caochenlei.codebuilder.entity.Table;
import com.caochenlei.codebuilder.entity.Template;
import com.caochenlei.db.util.JDBCUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Code {
	private static Logger logger = LoggerFactory.getLogger(Code.class);

	/**
	 * 功能说明：生成代码
	 * 
	 * @param database
	 * @param globalMap
	 */
	public static void create(Database database, Map<String, String> globalMap) {
		logger.info("============================================= * Create Code Start * ===========================================");
		// 获取数据库表集合
		List<Table> tables = database.getTables();

		// 获取对应模板文件
		List<Template> templateFiles = TemplateUtil.getAllTemplateFiles(globalMap.get("projectTemplatePath"));
		Map<String, String> tableTemplateMaps = TemplateUtil.getAllTemplateConfigs(globalMap.get("tableTemplatePath"));
		Map<String, String> columnTemplateMaps = TemplateUtil.getAllTemplateConfigs(globalMap.get("columnTemplatePath"));

		// 获取相对应的路径
		String createPath = globalMap.get("createPath");

		// 开始处理模板文件
		for (Template template : templateFiles) {
			// 判断当前模板文件是否需要处理
			if (TemplateUtil.isRenderFile(template.getName())) {
				// 读取模板文件
				String content = FileUtil.getContent(template.getAbsolutePath());

				// 替换表级模板
				content = TemplateUtil.createContentForTable(content, tableTemplateMaps, tables);

				// 如果文件名包含表替换符号则循环输出
				if (TemplateUtil.isContainsParams(template.getName())) {

					for (Table table : tables) {
						// 输出文件名称
						String outFile = ReflectUtil.assignment(template.getName(), ReflectUtil.parse(Table.class, new String[] { "columns" }), table);
						// 得到模板内容
						String outContent = content;
						// 替换列集模板
						outContent = TemplateUtil.createContentForColumn(outContent, columnTemplateMaps, table);
						// 替换全局参数
						outContent = TemplateUtil.createContentForGobal(outContent, globalMap);
						// 替换表级参数
						outContent = ReflectUtil.assignment(outContent, ReflectUtil.parse(Table.class, new String[] { "columns" }), table);
						// 输出文件名称
						String outPath = TemplateUtil.createContentForGobal(createPath + template.getRelativePath() + "\\" + outFile, globalMap).replace("(TFILE)", "");
						// 保存目的文件
						FileUtil.setContent(outPath, outContent);

						logger.info("正在渲染模板文件：" + outPath);
					}

				} else {
					// 输出文件名称
					String outPath = TemplateUtil.createContentForGobal(createPath + template.getRelativePath() + "\\" + template.getName(), globalMap).replace("(TFILE)", "");
					// 输出文件内容
					String outContent = TemplateUtil.createContentForGobal(content, globalMap);
					// 保存目的文件
					FileUtil.setContent(outPath, outContent);

					logger.info("正在渲染模板文件：" + outPath);
				}

			} else {
				String newPath = TemplateUtil.createContentForGobal(globalMap.get("createPath") + template.getRelativePath() + "\\" + template.getName(), globalMap);
				FileUtil.copyFile(template.getAbsolutePath(), newPath);

				logger.info("正在拷贝普通文件：" + newPath);
			}
		}

		logger.info("============================================= * Create Code End * =============================================");
	}

}
