package com.caochenlei.codebuilder.utils;

import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.*;

public class FileUtil {

	/**
	 * 功能说明：读取文件内容
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getContent(String fileName) {
		StringBuilder sb = new StringBuilder();

		try {
			File file = new File(fileName);
			InputStream inputStream = new FileInputStream(file);
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
				sb.append("\r\n");
			}

			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
		} catch (FileNotFoundException e) {
			System.out.println("读取文件失败：" + new File(fileName).getAbsolutePath());
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			System.out.println("读取文件失败：" + new File(fileName).getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("读取文件失败：" + new File(fileName).getAbsolutePath());
			e.printStackTrace();
		}

		return sb.toString();
	}

	/**
	 * 功能说明：设置文件内容
	 * 
	 * @param filePath
	 * @param content
	 */
	public static void setContent(String filePath, String content) {
		try {
			File file = new File(filePath);
			if (file.exists()) {
				file.delete();
			}

			File parentFile = file.getParentFile();
			if (!parentFile.exists()) {
				parentFile.mkdirs();
			}
			file.createNewFile();

			OutputStream outputStream = new FileOutputStream(file);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
			BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

			bufferedWriter.write(content);

			bufferedWriter.close();
			outputStreamWriter.close();
			outputStream.close();
		} catch (IOException e) {
			System.out.println("写出文件失败：" + new File(filePath).getAbsolutePath());
			e.printStackTrace();
		}
	}

	/**
	 * 功能说明：获取文件拓展名
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getExtendedName(String fileName) {
		if ((fileName != null) && (fileName.length() > 0)) {
			int dot = fileName.lastIndexOf('.');
			if ((dot > -1) && (dot < (fileName.length() - 1))) {
				return fileName.substring(dot + 1);
			}
		}
		return fileName;
	}

	/**
	 * 功能说明：拷贝单个文件
	 * 
	 * @param srcPath
	 * @param tarPath
	 */
	public static void copyFile(String srcPath, String tarPath) {
		try {
			int byteCount = 0;
			byte[] buffer = new byte[1024 * 4];

			File sFile = new File(srcPath);
			File tFile = new File(tarPath);

			if (sFile.exists()) {
				if (!tFile.getParentFile().exists()) {
					tFile.getParentFile().mkdirs();
				}

				InputStream inputStream = new FileInputStream(sFile);
				OutputStream outputStream = new FileOutputStream(tFile);

				while ((byteCount = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, byteCount);
				}

				outputStream.close();
				inputStream.close();
			}
		} catch (FileNotFoundException e) {
			System.out.println("拷贝单个文件失败：" + new File(srcPath).getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("拷贝单个文件失败：" + new File(srcPath).getAbsolutePath());
			e.printStackTrace();
		}
	}

	/**
	 * 功能说明：目录对话框
	 * 
	 * @param title
	 * @param defaultPath
	 * @return
	 */
	public static String selectDirPath(String title, String defaultPath) {
		// 修改主题
		String lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
		try {
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		// 选择目录
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.showDialog(new JLabel(), title);
		File file = fileChooser.getSelectedFile();
		// 默认目录
		if (file == null) {
			// 原来目录
			if (!"".equals(defaultPath)) {
				return defaultPath;
			}
			// 默认目录
			return SystemUtil.getDefaultCreatePath();
		}
		// 返回目录
		return file.getAbsolutePath();
	}

}
