package com.caochenlei.codebuilder.entity;

import java.util.List;

public class Database {

	private String dbIp;// 数据库IP
	private String dbPort;// 数据库端口
	private String dbName;// 数据库名称
	private String dbUrl;// 数据库地址
	private String dbUser;// 数据库用户
	private String dbPassword;// 数据库密码
	private String dbType;// 数据库类型
	private String dbDriverName;// 数据库驱动
	private String dbDataSourceType;// 数据库数据源类型
	private String dbDialect;// 数据库方言（仅HIBERNATE使用此字段）
	private String dbGenerator;// 数据库主键生成策略（仅HIBERNATE使用此字段）
	private List<Table> tables;// 表集合

	public String getDbIp() {
		return dbIp;
	}

	public void setDbIp(String dbIp) {
		this.dbIp = dbIp;
	}

	public String getDbPort() {
		return dbPort;
	}

	public void setDbPort(String dbPort) {
		this.dbPort = dbPort;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public String getDbDriverName() {
		return dbDriverName;
	}

	public void setDbDriverName(String dbDriverName) {
		this.dbDriverName = dbDriverName;
	}

	public String getDbDataSourceType() {
		return dbDataSourceType;
	}

	public void setDbDataSourceType(String dbDataSourceType) {
		this.dbDataSourceType = dbDataSourceType;
	}

	public String getDbDialect() {
		return dbDialect;
	}

	public void setDbDialect(String dbDialect) {
		this.dbDialect = dbDialect;
	}

	public String getDbGenerator() {
		return dbGenerator;
	}

	public void setDbGenerator(String dbGenerator) {
		this.dbGenerator = dbGenerator;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	@Override
	public String toString() {
		return "Database [dbIp=" + dbIp + ", dbPort=" + dbPort + ", dbName=" + dbName + ", dbUrl=" + dbUrl + ", dbUser=" + dbUser + ", dbPassword=" + dbPassword + ", dbType=" + dbType + ", dbDriverName=" + dbDriverName + ", dbDataSourceType=" + dbDataSourceType + ", dbDialect=" + dbDialect + ", dbGenerator=" + dbGenerator + ", tables=" + tables + "]";
	}

}
