package com.caochenlei.codebuilder.entity;

public class Template {

	private String name;// 模板名称
	private String absolutePath;// 模板绝对路径
	private String relativePath;// 模板相对路径

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	@Override
	public String toString() {
		return "Template [name=" + name + ", absolutePath=" + absolutePath + ", relativePath=" + relativePath + "]";
	}

}
