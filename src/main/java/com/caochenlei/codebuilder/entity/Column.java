package com.caochenlei.codebuilder.entity;

public class Column {

	private String columnName;// 列名称
	private String columnComment;// 列描述
	private String columnType;// 字段，数据库类型
	private String columnLanguageType;// 字段，编程语言类型
	private String column1;// 字段名，小写开头
	private String Column1Upper;// 字段名，大写开头
	private String column2;// 字段名，小写开头，驼峰处理
	private String Column2Upper;// 字段名，大写开头，驼峰处理
	private String column3;// 字段名，小写开头，去除前缀，驼峰处理
	private String Column3Upper;// 字段名，大写开头，去除前缀，驼峰处理
	private Boolean isPrimaryKey = false;// 状态值，判断该列是否为主键
	private Boolean isUniqueKey = false;// 状态值，判断该列是否为唯一键

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnComment() {
		return columnComment;
	}

	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getColumnLanguageType() {
		return columnLanguageType;
	}

	public void setColumnLanguageType(String columnLanguageType) {
		this.columnLanguageType = columnLanguageType;
	}

	public String getColumn1() {
		return column1;
	}

	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public String getColumn1Upper() {
		return Column1Upper;
	}

	public void setColumn1Upper(String column1Upper) {
		Column1Upper = column1Upper;
	}

	public String getColumn2() {
		return column2;
	}

	public void setColumn2(String column2) {
		this.column2 = column2;
	}

	public String getColumn2Upper() {
		return Column2Upper;
	}

	public void setColumn2Upper(String column2Upper) {
		Column2Upper = column2Upper;
	}

	public String getColumn3() {
		return column3;
	}

	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumn3Upper() {
		return Column3Upper;
	}

	public void setColumn3Upper(String column3Upper) {
		Column3Upper = column3Upper;
	}

	public Boolean getIsPrimaryKey() {
		return isPrimaryKey;
	}

	public void setIsPrimaryKey(Boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public Boolean getIsUniqueKey() {
		return isUniqueKey;
	}

	public void setIsUniqueKey(Boolean uniqueKey) {
		isUniqueKey = uniqueKey;
	}

	@Override
	public String toString() {
		return "Column{" +
				"columnName='" + columnName + '\'' +
				", columnComment='" + columnComment + '\'' +
				", columnType='" + columnType + '\'' +
				", columnLanguageType='" + columnLanguageType + '\'' +
				", column1='" + column1 + '\'' +
				", Column1Upper='" + Column1Upper + '\'' +
				", column2='" + column2 + '\'' +
				", Column2Upper='" + Column2Upper + '\'' +
				", column3='" + column3 + '\'' +
				", Column3Upper='" + Column3Upper + '\'' +
				", isPrimaryKey=" + isPrimaryKey +
				", isUniqueKey=" + isUniqueKey +
				'}';
	}
}
