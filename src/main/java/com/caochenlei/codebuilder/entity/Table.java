package com.caochenlei.codebuilder.entity;

import java.util.List;

public class Table {

	private String tableName;// 表名称
	private String tableComment;// 表描述
	private String table1;// 表名，小写开头
	private String Table1Upper;// 表名，大写开头
	private String table2;// 表名，小写开头，驼峰格式
	private String Table2Upper;// 表名，大写开头，驼峰格式
	private String table3;// 表名，小写开头，去除前缀，驼峰格式
	private String Table3Upper;// 表名，大写开头，去除前缀，驼峰格式

	private String tablePrimaryKeyName;// 表主键，名称
	private String tablePrimaryKeyComment;// 表主键，备注
	private String tablePrimaryKeyType;// 表主键，数据库类型
	private String tablePrimaryKeyLanguageType;// 表主键，编程语言类型
	private String tablePrimary1;// 表主键，小写开头
	private String TablePrimary1Upper;// 表主键，大写开头
	private String tablePrimary2;// 表主键，小写开头，驼峰格式
	private String TablePrimary2Upper;// 表主键，大写开头，驼峰格式
	private String tablePrimary3;// 表主键，小写开头，去除前缀，驼峰格式
	private String TablePrimary3Upper;// 表主键，大写开头，去除前缀，驼峰格式

	private List<Column> columns;// 列集合

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableComment() {
		return tableComment;
	}

	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

	public String getTable1() {
		return table1;
	}

	public void setTable1(String table1) {
		this.table1 = table1;
	}

	public String getTable1Upper() {
		return Table1Upper;
	}

	public void setTable1Upper(String table1Upper) {
		Table1Upper = table1Upper;
	}

	public String getTable2() {
		return table2;
	}

	public void setTable2(String table2) {
		this.table2 = table2;
	}

	public String getTable2Upper() {
		return Table2Upper;
	}

	public void setTable2Upper(String table2Upper) {
		Table2Upper = table2Upper;
	}

	public String getTable3() {
		return table3;
	}

	public void setTable3(String table3) {
		this.table3 = table3;
	}

	public String getTable3Upper() {
		return Table3Upper;
	}

	public void setTable3Upper(String table3Upper) {
		Table3Upper = table3Upper;
	}

	public String getTablePrimaryKeyName() {
		return tablePrimaryKeyName;
	}

	public void setTablePrimaryKeyName(String tablePrimaryKeyName) {
		this.tablePrimaryKeyName = tablePrimaryKeyName;
	}

	public String getTablePrimaryKeyComment() {
		return tablePrimaryKeyComment;
	}

	public void setTablePrimaryKeyComment(String tablePrimaryKeyComment) {
		this.tablePrimaryKeyComment = tablePrimaryKeyComment;
	}

	public String getTablePrimaryKeyType() {
		return tablePrimaryKeyType;
	}

	public void setTablePrimaryKeyType(String tablePrimaryKeyType) {
		this.tablePrimaryKeyType = tablePrimaryKeyType;
	}

	public String getTablePrimaryKeyLanguageType() {
		return tablePrimaryKeyLanguageType;
	}

	public void setTablePrimaryKeyLanguageType(String tablePrimaryKeyLanguageType) {
		this.tablePrimaryKeyLanguageType = tablePrimaryKeyLanguageType;
	}

	public String getTablePrimary1() {
		return tablePrimary1;
	}

	public void setTablePrimary1(String tablePrimary1) {
		this.tablePrimary1 = tablePrimary1;
	}

	public String getTablePrimary1Upper() {
		return TablePrimary1Upper;
	}

	public void setTablePrimary1Upper(String tablePrimary1Upper) {
		TablePrimary1Upper = tablePrimary1Upper;
	}

	public String getTablePrimary2() {
		return tablePrimary2;
	}

	public void setTablePrimary2(String tablePrimary2) {
		this.tablePrimary2 = tablePrimary2;
	}

	public String getTablePrimary2Upper() {
		return TablePrimary2Upper;
	}

	public void setTablePrimary2Upper(String tablePrimary2Upper) {
		TablePrimary2Upper = tablePrimary2Upper;
	}

	public String getTablePrimary3() {
		return tablePrimary3;
	}

	public void setTablePrimary3(String tablePrimary3) {
		this.tablePrimary3 = tablePrimary3;
	}

	public String getTablePrimary3Upper() {
		return TablePrimary3Upper;
	}

	public void setTablePrimary3Upper(String tablePrimary3Upper) {
		TablePrimary3Upper = tablePrimary3Upper;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	@Override
	public String toString() {
		return "Table [tableName=" + tableName + ", tableComment=" + tableComment + ", table1=" + table1 + ", Table1Upper=" + Table1Upper + ", table2=" + table2 + ", Table2Upper=" + Table2Upper + ", table3=" + table3 + ", Table3Upper=" + Table3Upper + ", tablePrimaryKeyName=" + tablePrimaryKeyName + ", tablePrimaryKeyComment=" + tablePrimaryKeyComment + ", tablePrimaryKeyType=" + tablePrimaryKeyType + ", tablePrimaryKeyLanguageType=" + tablePrimaryKeyLanguageType + ", tablePrimary1=" + tablePrimary1
				+ ", TablePrimary1Upper=" + TablePrimary1Upper + ", tablePrimary2=" + tablePrimary2 + ", TablePrimary2Upper=" + TablePrimary2Upper + ", tablePrimary3=" + tablePrimary3 + ", TablePrimary3Upper=" + TablePrimary3Upper + ", columns=" + columns + "]";
	}

}
