package com.caochenlei.db.util;

import java.util.regex.Pattern;

public final class Utility {
	private static final Pattern isAllWhitespacePattern = Pattern.compile("^\\s*$");

	public static boolean isBlank(final String text) {
		return text == null || text.isEmpty() || isAllWhitespacePattern.matcher(text).matches();
	}

	public static String quote(String s) {
		return "'" + s + "'";
	}
}