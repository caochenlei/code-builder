package com.caochenlei.db.util;

import java.util.Map;

import com.caochenlei.db.meta.schema.Column;
import com.caochenlei.db.meta.schema.Table;

public class PrintUtils {
	public static void printTable(Table table) {
		Map<String, Column> columns = table.getColumns();
		System.out.println("【" + table.getName() + "】" + " : " + table.getComment());
		for (String column : columns.keySet()) {
			System.out.println(column + " : " + columns.get(column));
		}
		System.out.println();
	}
}
