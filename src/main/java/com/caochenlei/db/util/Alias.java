package com.caochenlei.db.util;

/**
 * 功能说明：别名处理工具类
 *
 * @author CaoChenLei
 */
public class Alias {
	/**
	 * 功能说明：将字符串变成全部变成小写
	 */
	public static String toLower(String value) {
		if (isNotNull(value)) {
			return value.toLowerCase();
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：将字符串变成全部变成大写
	 */
	public static String toUpper(String value) {
		if (isNotNull(value)) {
			return value.toUpperCase();
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：判断字符串是否为空串
	 *
	 * @param value
	 * @return
	 */
	public static boolean isBlank(String value) {
		if (isNotNull(value)) {
			return !(value.length() > 0);
		} else {
			return false;
		}
	}

	/**
	 * 功能说明：判断字符串是否不为空串
	 *
	 * @param value
	 * @return
	 */
	public static boolean isNotBlank(String value) {
		if (isNotNull(value)) {
			return value.length() > 0;
		} else {
			return false;
		}
	}

	/**
	 * 功能说明：判断对象是否为空对象
	 *
	 * @param value
	 * @return
	 */
	public static boolean isNull(String value) {
		return value == null;
	}

	/**
	 * 功能说明：判断对象是否不为空对象
	 *
	 * @param value
	 * @return
	 */
	public static boolean isNotNull(String value) {
		return !(value == null);
	}

	/**
	 * 功能说明：小写开头
	 *
	 * @param value
	 * @return
	 */
	public static String getLower(String value) {
		if (isNotNull(value)) {
			return value.trim();
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：大写开头
	 *
	 * @param value
	 * @return
	 */
	public static String getUpper(String value) {
		if (isNotNull(value)) {
			String s0 = value.substring(0, 1).trim().toUpperCase();
			return s0 + value.substring(1);
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：小写开头，驼峰格式
	 *
	 * @param value
	 * @return
	 */
	public static String getLowerAndCamel(String value) {
		if (isNotNull(value)) {
			while (true) {
				int index = value.indexOf("_");
				if (index == -1) {
					break;
				}
				String name = value.substring(index + 1, index + 2).toUpperCase();
				value = value.substring(0, index) + name + value.substring(index + 2);
			}
			return value;
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：大写开头，驼峰格式
	 *
	 * @param value
	 * @return
	 */
	public static String getUpperAndCamel(String value) {
		if (isNotNull(value)) {
			return getUpper(getLowerAndCamel(value));
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：小写开头，去除前缀，驼峰格式
	 *
	 * @param value
	 * @return
	 */
	public static String getLowerAndClearPrefixAndCamel(String value) {
		if (isNotNull(value)) {
			int index = value.indexOf("_");
			if (index == -1) {
				return value;
			}
			return getLowerAndCamel(value.substring(index + 1));
		} else {
			return "";
		}
	}

	/**
	 * 功能说明：大写开头，去除前缀，驼峰格式
	 *
	 * @param value
	 * @return
	 */
	public static String getUpperAndClearPrefixAndCamel(String value) {
		if (isNotNull(value)) {
			return getUpper(getLowerAndClearPrefixAndCamel(value));
		} else {
			return "";
		}
	}
}
