package com.caochenlei.db.meta.schema;

import java.io.Serializable;
import java.util.Set;

public class Database implements Serializable {
	private static final long serialVersionUID = 4791419417571119610L;
	private DatabaseInfo databaseInfo;
	private Set<Schema> schemas;

	public DatabaseInfo getDatabaseInfo() {
		return databaseInfo;
	}

	public void setDatabaseInfo(DatabaseInfo databaseInfo) {
		this.databaseInfo = databaseInfo;
	}

	public Set<Schema> getSchemas() {
		return schemas;
	}

	public void setSchemas(Set<Schema> schemas) {
		this.schemas = schemas;
	}

	@Override
	public String toString() {
		return "Database [databaseInfo=" + databaseInfo + ", schemas=" + schemas + "]";
	}
}
