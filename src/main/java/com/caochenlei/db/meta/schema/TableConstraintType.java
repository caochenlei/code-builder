package com.caochenlei.db.meta.schema;

public enum TableConstraintType {
	unknown("unknown"), 
	check("CHECK"), 
	unique("UNIQUE");

	public static TableConstraintType valueOfFromValue(final String value) {
		for (final TableConstraintType type : TableConstraintType.values()) {
			if (type.getValue().equalsIgnoreCase(value)) {
				return type;
			}
		}
		return unknown;
	}

	private final String value;

	private TableConstraintType(final String value) {
		this.value = value;
	}

	public final String getValue() {
		return value;
	}
}
