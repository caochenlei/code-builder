package com.caochenlei.db.meta.schema;

import java.io.Serializable;
import java.util.Map;

public class Schema implements Serializable {
	private static final long serialVersionUID = 7247506286961678313L;
	private SchemaInfo schemaInfo;
	private Map<String, Table> tables;

	public SchemaInfo getSchemaInfo() {
		return schemaInfo;
	}

	public void setSchemaInfo(SchemaInfo schemaInfo) {
		this.schemaInfo = schemaInfo;
	}

	public Map<String, Table> getTables() {
		return tables;
	}

	public void setTables(Map<String, Table> tables) {
		this.tables = tables;
	}

	@Override
	public String toString() {
		return "Schema [schemaInfo=" + schemaInfo + ", tables=" + tables + "]";
	}
}
