package com.caochenlei.db.meta.schema;

import java.sql.DatabaseMetaData;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum ForeignKeyDeferrability {
	unknown(-1, "unknown"), 
	initiallyDeferred(DatabaseMetaData.importedKeyInitiallyDeferred, "initially deferred"), 
	initiallyImmediate(DatabaseMetaData.importedKeyInitiallyImmediate, "initially immediate"), 
	keyNotDeferrable(DatabaseMetaData.importedKeyNotDeferrable, "not deferrable");

	private static final Logger LOGGER = Logger.getLogger(ForeignKeyDeferrability.class.getName());

	public static ForeignKeyDeferrability valueOf(final int id) {
		for (final ForeignKeyDeferrability fkDeferrability : ForeignKeyDeferrability.values()) {
			if (fkDeferrability.getId() == id) {
				return fkDeferrability;
			}
		}
		LOGGER.log(Level.FINE, "Unknown id " + id);
		return unknown;
	}

	private final int id;
	private final String text;

	private ForeignKeyDeferrability(final int id, final String text) {
		this.id = id;
		this.text = text;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return text;
	}
}
