package com.caochenlei.db.meta.schema;

import java.sql.DatabaseMetaData;

public enum IndexType {
	statistic(DatabaseMetaData.tableIndexStatistic), 
	clustered(DatabaseMetaData.tableIndexClustered), 
	hashed(DatabaseMetaData.tableIndexHashed), 
	other(DatabaseMetaData.tableIndexOther);

	public static IndexType valueOf(final int id) {
		for (final IndexType type : IndexType.values()) {
			if (type.getId() == id) {
				return type;
			}
		}
		return other;
	}

	private final int id;

	private IndexType(final int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
