package com.caochenlei.db.meta.schema;

import java.io.Serializable;

public class Constraint implements Serializable {
	private static final long serialVersionUID = 6464975225064851090L;
	private String name;
	private TableConstraintType tableConstraintType;
	private boolean deferrable;
	private String definition;// such as "D1 IS NOT NULL"

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TableConstraintType getTableConstraintType() {
		return tableConstraintType;
	}

	public void setTableConstraintType(TableConstraintType tableConstraintType) {
		this.tableConstraintType = tableConstraintType;
	}

	public boolean isDeferrable() {
		return deferrable;
	}

	public void setDeferrable(boolean deferrable) {
		this.deferrable = deferrable;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	@Override
	public String toString() {
		return "Constraint [name=" + name + ", tableConstraintType=" + tableConstraintType + ", deferrable=" + deferrable + ", definition=" + definition + "]";
	}
}
