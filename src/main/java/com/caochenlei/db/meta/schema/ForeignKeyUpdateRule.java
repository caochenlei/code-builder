package com.caochenlei.db.meta.schema;

import java.sql.DatabaseMetaData;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum ForeignKeyUpdateRule {
	unknown(-1, "unknown"), 
	noAction(DatabaseMetaData.importedKeyNoAction, "no action"), 
	cascade(DatabaseMetaData.importedKeyCascade, "cascade"), 
	setNull(DatabaseMetaData.importedKeySetNull, "set null"), 
	setDefault(DatabaseMetaData.importedKeySetDefault, "set default"), 
	restrict(DatabaseMetaData.importedKeyRestrict, "restrict");

	private static final Logger LOGGER = Logger.getLogger(ForeignKeyUpdateRule.class.getName());

	public static ForeignKeyUpdateRule valueOf(final int id) {
		for (final ForeignKeyUpdateRule type : ForeignKeyUpdateRule.values()) {
			if (type.getId() == id) {
				return type;
			}
		}
		LOGGER.log(Level.FINE, "Unknown id " + id);
		return unknown;
	}

	private final String text;
	private final int id;

	private ForeignKeyUpdateRule(final int id, final String text) {
		this.id = id;
		this.text = text;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return text;
	}
}
