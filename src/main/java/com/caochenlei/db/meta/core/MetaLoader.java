package com.caochenlei.db.meta.core;

import java.util.Map;
import java.util.Set;

import com.caochenlei.db.exception.DataAccessException;
import com.caochenlei.db.meta.schema.Database;
import com.caochenlei.db.meta.schema.Function;
import com.caochenlei.db.meta.schema.Procedure;
import com.caochenlei.db.meta.schema.Schema;
import com.caochenlei.db.meta.schema.SchemaInfo;
import com.caochenlei.db.meta.schema.Table;
import com.caochenlei.db.meta.schema.Trigger;

public interface MetaLoader {
	Set<String> getTableNames() throws DataAccessException;

	Table getTable(String tableName) throws DataAccessException;

	Table getTable(String tableName, SchemaInfoLevel schemaLevel) throws DataAccessException;

	Table getTable(String tableName, SchemaInfo schemaInfo) throws DataAccessException;

	Set<SchemaInfo> getSchemaInfos() throws DataAccessException;

	Schema getSchema() throws DataAccessException;

	Schema getSchema(SchemaInfo schemaInfo) throws DataAccessException;

	Schema getSchema(SchemaInfoLevel level) throws DataAccessException;

	Schema getSchema(SchemaInfo schemaInfo, SchemaInfoLevel level) throws DataAccessException;

	Set<String> getProcedureNames() throws DataAccessException;

	Procedure getProcedure(String procedureName) throws DataAccessException;

	Map<String, Procedure> getProcedures() throws DataAccessException;

	Set<String> getTriggerNames() throws DataAccessException;

	Trigger getTrigger(String triggerName) throws DataAccessException;

	Map<String, Trigger> getTriggers() throws DataAccessException;

	Set<String> getFunctionNames() throws DataAccessException;

	Function getFunction(String name) throws DataAccessException;

	Map<String, Function> getFunctions() throws DataAccessException;

	@Deprecated
	Database getDatabase() throws DataAccessException;

	@Deprecated
	Database getDatabase(SchemaInfoLevel level) throws DataAccessException;
}
