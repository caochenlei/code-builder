package com.caochenlei.db.meta.core;

import java.sql.Connection;

import com.caochenlei.db.meta.retriever.MetaCrawler;

public interface MetaCrawlerFactory {
	MetaCrawler newInstance(Connection con);
}