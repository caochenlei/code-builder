package com.caochenlei.db.meta.retriever;

import java.util.Map;
import java.util.Set;
import com.caochenlei.db.meta.core.SchemaInfoLevel;
import com.caochenlei.db.meta.schema.Database;
import com.caochenlei.db.meta.schema.DatabaseInfo;
import com.caochenlei.db.meta.schema.Function;
import com.caochenlei.db.meta.schema.Procedure;
import com.caochenlei.db.meta.schema.Schema;
import com.caochenlei.db.meta.schema.SchemaInfo;
import com.caochenlei.db.meta.schema.Table;
import com.caochenlei.db.meta.schema.Trigger;

public interface MetaCrawler {
	Set<String> getTableNames();

	Table getTable(String tableName, SchemaInfoLevel schemaInfoLevel);

	Table getTable(String tableName, SchemaInfoLevel level, SchemaInfo schemaInfo);

	Set<SchemaInfo> getSchemaInfos();

	Schema getSchema(SchemaInfoLevel level);

	Schema getSchema(SchemaInfo schemaInfo, SchemaInfoLevel level);

	DatabaseInfo getDatabaseInfo();

	Database getDatabase(SchemaInfoLevel level);

	Set<String> getProcedureNames(SchemaInfo schemaInfo);

	Procedure getProcedure(String procedureName);

	Map<String, Procedure> getProcedures();

	Set<String> getTriggerNames();

	Trigger getTrigger(String triggerName);

	Map<String, Trigger> getTriggers();

	Set<String> getFunctionNames();

	Function getFunction(String name);

	Map<String, Function> getFunctions();
}
