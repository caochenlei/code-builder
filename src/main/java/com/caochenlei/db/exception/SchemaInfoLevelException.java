package com.caochenlei.db.exception;

import java.sql.SQLException;

public class SchemaInfoLevelException extends RuntimeException {
	private static final long serialVersionUID = 7172269864632387030L;

	public SchemaInfoLevelException(String msg) {
		super(msg);
	}

	public SchemaInfoLevelException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public SchemaInfoLevelException(String msg, SQLException ex) {
		super(msg, ex);
	}
}
