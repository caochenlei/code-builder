package com.caochenlei.db.exception;

import java.sql.SQLException;

public class NonTransientDataAccessException extends DataAccessException {
	private static final long serialVersionUID = -1845872097313221377L;

	public NonTransientDataAccessException(String msg) {
		super(msg);
	}

	public NonTransientDataAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NonTransientDataAccessException(String msg, SQLException ex) {
		super(msg, ex);
	}
}
