package com.caochenlei.db.exception;

import java.sql.SQLException;

public class CannotGetJdbcConnectionException extends NestedRuntimeException {
	private static final long serialVersionUID = -3949687968948707037L;

	public CannotGetJdbcConnectionException(String msg) {
		super(msg);
	}

	public CannotGetJdbcConnectionException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public CannotGetJdbcConnectionException(String msg, SQLException ex) {
		super(msg, ex);
	}
}