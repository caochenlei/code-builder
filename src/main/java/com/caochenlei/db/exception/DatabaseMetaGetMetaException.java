package com.caochenlei.db.exception;

import java.sql.SQLException;

public class DatabaseMetaGetMetaException extends DataAccessException {
	private static final long serialVersionUID = -3425469780922160793L;

	public DatabaseMetaGetMetaException(String msg) {
		super(msg);
	}

	public DatabaseMetaGetMetaException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public DatabaseMetaGetMetaException(String msg, SQLException ex) {
		super(msg, ex);
	}
}