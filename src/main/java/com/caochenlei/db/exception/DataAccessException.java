package com.caochenlei.db.exception;

import java.sql.SQLException;

public abstract class DataAccessException extends NestedRuntimeException {
	private static final long serialVersionUID = -6520767808362481227L;

	public DataAccessException(String msg) {
		super(msg);
	}

	public DataAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public DataAccessException(String msg, SQLException ex) {
		super(msg, ex);
	}
}